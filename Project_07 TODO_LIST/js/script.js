/// <--- FIRST METHOD ---> ///
// $('li').on('click' , function() {
//     if ($(this).css('color') === 'rgb(128, 128, 128)') {//if li grey
//         $(this).css({//change to black
//             color: 'black',
//             textDecoration: 'none'
//         });
//     }
//     else { //else li is black
//         $(this).css({
//             color: 'gray',
//             textDecoration: 'line-through'
//         });
//     }
// })


/// <--- SECOND METHOD ---> ///
$('ul').on('click',"li", function(){
    $(this).toggleClass('gray')
})

$('ul').on('click',"span",function(event){
    $(this).parent().fadeOut(1000,function(){
        $(this).remove()
    })
    event.stopPropagation()
})

$('input').on('keypress', function(event){
    if (event.which === 13) {
        let newTodo = $(this).val()
        $(this).val('')
        $('ul').append(`<li><span><i class="far fa-times-circle"></i></span> ${newTodo}</li>`)
    }
})