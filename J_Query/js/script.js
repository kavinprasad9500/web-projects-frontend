//// <--- STYLE USING JQUERY--->  ////
// let style = {
//     'background': 'red',
//     'fontSize': '230px',
//     'border': '3px solid blue'
// }
// $('h1').css(style);

//// <--- STYLE USING JAVA SCRIPT--->  ////
// document.querySelector('h1').textContent = 'Changed By Js';
// document.querySelector('h1').style.background = 'red';



//// <--- CHANGE TEXT USING JQUERY--->  ////
// $('#id1').text('changed')
// $('.class1').text('changed')




//// <--- CHNAGE HTML TAGS AND TEXT USING JQUERY--->  ////
// $('li').html('<a href="https://www.google.com/">google</a>')
// $('li').text('<a href="https://www.google.com/">google</a>')




//// <--- CHANGE COLOR USING JQUERY--->  ////
// $('li').css('color','red')

//// <--- CHANGE COLOR USING JAVA SCRIPT--->  ////
// let lis = document.querySelectorAll('li');
// lis.forEach ((li) => li.style.color = 'red');



////// <--- ATTRIBUTES AND VAL ---> //////
// CHANGE IMAGES SIZES 
// $('img').attr('width' , '500px')
// CHANGE IMAGE 
// $('img').attr('src' , 'https://cdn.pixabay.com/photo/2021/07/19/20/11/kitten-6479019_960_720.jpg')

// EDIT INPUT TYPES 
// $('input').attr('value','changed')
// $('input').val('changed By Val')

// ADD CLASSES
$('input').addClass('ADDED')
// REMOVE CLASSES
$('h1').removeClass('Added')