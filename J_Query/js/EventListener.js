// <--- ONCLICK WITH JQUERY --->
// $('h1').click(function() {
//     $(this).css('background','pink')
// })

// <--- ONCLICK WITH JAVA SCRIPT --->
// document.querySelector('h1').addEventListener('click',() => {
//     console.log('Clicked');
// })


// <--- KEY PRESS --->//// INPUT TRACKING  /////
// $('input').keypress(function(event){
//     console.log(event.target.value);
// })

/// <--- ON EVENT LISTENER ---> ///
// $('h1').on('click', () => {
//     console.log('Pressed');
// })
// $('h1').on('mouseover', () => {
//     console.log('Pressed');
// })
$('h1').on('mouseover', function() {
    $(this).css('font-weight','bold');
})